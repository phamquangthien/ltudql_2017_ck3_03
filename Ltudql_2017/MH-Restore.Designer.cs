﻿namespace Ltudql_2017
{
    partial class MH_Restore
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.restorebtn = new System.Windows.Forms.Button();
            this.browsebtn2 = new System.Windows.Forms.Button();
            this.pathtxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.trangthai = new System.Windows.Forms.Label();
            this.state = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Crimson;
            this.label2.Location = new System.Drawing.Point(24, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(565, 76);
            this.label2.TabIndex = 24;
            this.label2.Text = "Phục Hồi Dữ Liệu";
            // 
            // restorebtn
            // 
            this.restorebtn.Enabled = false;
            this.restorebtn.Location = new System.Drawing.Point(734, 242);
            this.restorebtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.restorebtn.Name = "restorebtn";
            this.restorebtn.Size = new System.Drawing.Size(146, 50);
            this.restorebtn.TabIndex = 28;
            this.restorebtn.Text = "ResTore";
            this.restorebtn.UseVisualStyleBackColor = true;
            this.restorebtn.Click += new System.EventHandler(this.restorebtn_Click);
            // 
            // browsebtn2
            // 
            this.browsebtn2.Location = new System.Drawing.Point(734, 169);
            this.browsebtn2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.browsebtn2.Name = "browsebtn2";
            this.browsebtn2.Size = new System.Drawing.Size(146, 50);
            this.browsebtn2.TabIndex = 27;
            this.browsebtn2.Text = "Browse";
            this.browsebtn2.UseVisualStyleBackColor = true;
            this.browsebtn2.Click += new System.EventHandler(this.browsebtn2_Click);
            // 
            // pathtxt
            // 
            this.pathtxt.Location = new System.Drawing.Point(348, 188);
            this.pathtxt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pathtxt.Name = "pathtxt";
            this.pathtxt.Size = new System.Drawing.Size(350, 31);
            this.pathtxt.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(234, 196);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 25);
            this.label1.TabIndex = 25;
            this.label1.Text = "Location";
            // 
            // trangthai
            // 
            this.trangthai.AutoSize = true;
            this.trangthai.Location = new System.Drawing.Point(344, 360);
            this.trangthai.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.trangthai.Name = "trangthai";
            this.trangthai.Size = new System.Drawing.Size(171, 25);
            this.trangthai.TabIndex = 32;
            this.trangthai.Text = "Chưa Hoạt Động";
            // 
            // state
            // 
            this.state.AutoSize = true;
            this.state.Location = new System.Drawing.Point(244, 360);
            this.state.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.state.Name = "state";
            this.state.Size = new System.Drawing.Size(74, 25);
            this.state.TabIndex = 31;
            this.state.Text = "State: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(466, 315);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 25);
            this.label3.TabIndex = 30;
            this.label3.Text = "0%";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(348, 242);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(350, 50);
            this.progressBar1.TabIndex = 29;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MH_Restore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.trangthai);
            this.Controls.Add(this.state);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.restorebtn);
            this.Controls.Add(this.browsebtn2);
            this.Controls.Add(this.pathtxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MH_Restore";
            this.Size = new System.Drawing.Size(1474, 827);
            this.Load += new System.EventHandler(this.MH_Restore_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button restorebtn;
        private System.Windows.Forms.Button browsebtn2;
        private System.Windows.Forms.TextBox pathtxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label trangthai;
        private System.Windows.Forms.Label state;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Timer timer1;
    }
}
