﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ThamSoDAO
    {
        public int loadSoKhachNuocNgoai()
        {
            Provider provider = new Provider();
            provider.Connect();

            DataTable dt = new DataTable();
            int soKhachNuocNgoai = 0;
            string sqlQuery = "SELECT DISTINCT SoKhachNuocNgoai FROM THAMSO";

            dt = provider.Select(CommandType.Text, sqlQuery);

            soKhachNuocNgoai = dt.Rows[0].Field<int>(0);

            provider.DisConnect();

            return soKhachNuocNgoai;
        }

        public float loadTyLePhuThu()
        {
            Provider provider = new Provider();
            provider.Connect();

            DataTable dt = new DataTable();
            float tyLePhuThu = 0;
            string sqlQuery = "SELECT DISTINCT TiLePhuThu FROM THAMSO";

            dt = provider.Select(CommandType.Text, sqlQuery);

            tyLePhuThu = dt.Rows[0].Field<float>(0);

            provider.DisConnect();

            return tyLePhuThu;
        }
    }
}
