﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class Provider
    {

       //static string ConnectionString = @"Data Source=DESKTOP-0IEJSRU;Initial Catalog=QuanLyKhachSanDB;Integrated Security=True";//Đường dẫn kết nối với CSDL

        // Máy Trung 
       // static string ConnectionString = @"Data Source=DESKTOP-HUL2SIO;Initial Catalog=QuanLyKhachSan;Integrated Security=True";

        //Máy Trang
        //static string ConnectionString = @"Data Source=TRANGLUONG\SQLEXPRESS;Initial Catalog=QuanLyKhachSan;Integrated Security=True";//Đường dẫn kết nối với CSDL
        //static string ConnectionString = @"Server=66a0396c-a61c-4dbb-bae4-ab3e0069f477.sqlserver.sequelizer.com;Database=db66a0396ca61c4dbbbae4ab3e0069f477;User ID=mwoodzwjinipurue;Password=ToAqzNXd7zB4bTRCDvzaZKH6er6Gr4JbfYuHtqFWeCJPB6vJXYgBoVtiMCkiQqUc;";



        SqlConnection Connection { get; set; }
        public string TenDaTaBase()
        {
            return Connection.Database;
        }
        //Hàm kết Nối CSDL
        public void Connect()
        {
            try
            {
                var ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Ltudql_2017.Properties.Settings.QuanLyKhachSanDBConnectionString"].ConnectionString;
                if (Connection == null)
                    Connection = new SqlConnection(ConnectionString);
                if (Connection.State != ConnectionState.Closed)
                    Connection.Close();
                Connection.Open();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        //Hàm Bỏ Kết Nối CSDL
        public void DisConnect()
        {
            if (Connection != null && Connection.State == ConnectionState.Open)
                Connection.Close();
        }
        //Hàm Truy Vấn Vào CSDL
        // Tham Số đầu vào 
        //+ CommandType kiểu Text hay StoredProcedure
        //+ Câu truy ván sql
        //+Mảng SqlParamater tham số đầu vào sql
        //Hàm này để chỉnh sửa Với CSDL đầu ra kiểu int
        public int ExecuteNonQuery(CommandType cmtType, string strSql,
                            params SqlParameter[] parameters)
        {
            try
            {
                SqlCommand command = Connection.CreateCommand();
                command.CommandText = strSql;
                command.CommandType = cmtType;
                if (parameters != null && parameters.Length > 0)
                    command.Parameters.AddRange(parameters);
                int nRow = command.ExecuteNonQuery();
                return nRow;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        //Hàm Truy Vấn Vào CSDL
        // Tham Số đầu vào 
        //+ CommandType kiểu Text hay StoredProcedure
        //+ Câu truy ván sql
        //+Mảng SqlParamater tham số đầu vào sql
        //Hàm này để truy vấn lấy dữ liệu ra bỏ vào datatable
        public DataTable Select(CommandType cmtType, string strSql,
                            params SqlParameter[] parameters)
        {
            try
            {
                SqlCommand command = Connection.CreateCommand();
                command.CommandText = strSql;
                command.CommandType = cmtType;
                if (parameters != null && parameters.Length > 0)
                    command.Parameters.AddRange(parameters);

                SqlDataAdapter da = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }


    }
}
