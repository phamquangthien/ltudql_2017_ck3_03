﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class ImportPhongBUS
    {
        public string DuLieuMCuoiDanhSach()
        {
            ImportPhongDAO xl = new ImportPhongDAO();
            DataTable dt = xl.DuLieuMCuoiDanhSach();
            return dt.Rows[0][0].ToString();
        }

        public int ThemPhong(PhongDTO p)
        {
            

           
            ImportPhongDAO xl = new ImportPhongDAO();
            return xl.ThemPhong(p);



        }
        public DataTable DanhSachLoaiPhong()
        {
            ImportPhongDAO xl = new ImportPhongDAO();

            return xl.DanhSachLoaiPhong();
        }
        public DataTable DanhSachLoaiTinhTrang()
        {
            ImportPhongDAO xl = new ImportPhongDAO();

           
            return  xl.DanhSachLoaiTinhTrang();
        }
    }
}
