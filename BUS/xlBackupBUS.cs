﻿using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class xlBackupBUS
    {
        public int xlBackup(string path)
        {

            BackupDAO xl = new BackupDAO();
            return xl.xlBackup(path);

        }
    }
}
