﻿using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class PhongBUS
    {
        public DataTable loadPhongChuaThue()
        {
            PhongDAO phongDAO = new PhongDAO();
            return phongDAO.loadPhongChuaThue();
        }

        public DataTable loadPhongDaThue()
        {
            PhongDAO phongDAO = new PhongDAO();
            return phongDAO.loadPhongDaThue();
        }

        public string loadTenPhong(string maPhong)
        {
            PhongDAO phongDAO = new PhongDAO();
            return phongDAO.loadTenPhong(maPhong);
        }

        public DateTime loadNgayThuePhong(string maPhong)
        {
            PhongDAO phongDAO = new PhongDAO();
            return phongDAO.loadNgayThuePhong(maPhong);
        }

        public double loadDonGia(string maPhong)
        {
            PhongDAO phongDAO = new PhongDAO();
            return phongDAO.loadDonGia(maPhong);
        }

        public double loadPhuThuNuocNgoai(string maPhong)
        {
            PhongDAO phongDAO = new PhongDAO();
            return phongDAO.loadPhuThuNuocNgoai(maPhong);
        }

        public double loadTiLePhuThuNuocNgoai(string maPhong)
        {
            PhongDAO phongDAO = new PhongDAO();
            return phongDAO.loadTiLePhuThuNuocNgoai(maPhong);
        }
    }
}
