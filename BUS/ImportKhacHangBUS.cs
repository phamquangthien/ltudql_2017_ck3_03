﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class ImportKhacHangBUS
    {
        public string DuLieuMCuoiDanhSach()
        {
            ImportKhachHangDAO xl = new ImportKhachHangDAO();
            DataTable dt = xl.DuLieuMCuoiDanhSach();
            return dt.Rows[0][0].ToString();
        }
        public DataTable DanhSachLoaiKhachHang()
        {
            ImportKhachHangDAO xl = new ImportKhachHangDAO();
            return xl.DanhSachLoaiKhachHang();
        }
        public int ThemKhachHnag(KhachHangDTO kh)
        {
            ImportKhachHangDAO xl = new ImportKhachHangDAO();
            return xl.ThemKhachHnag(kh);
        }
    }
}
