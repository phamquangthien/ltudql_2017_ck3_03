﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class ThamSoBUS
    {
        public int loadSoKhachNuocNgoai()
        {
            ThamSoDAO dao = new ThamSoDAO();
            return dao.loadSoKhachNuocNgoai();
        }
        public float loadTyLePhuThu()
        {
            ThamSoDAO dao = new ThamSoDAO();
            return dao.loadTyLePhuThu();
        }
    }
}
