﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class xlTraCuuPhongBUS
    {

        public DataTable LoadTimKiem(PhongDTO p, LoaiPhongDTO lp, LoaiTinhTrangDTO tt)
        {
            xlTraCuuPhongDAO xl = new xlTraCuuPhongDAO();
            return xl.LoadTimKiem(p, lp, tt);
        }

        public DataTable LoadLoaiPhong()
        {
            xlTraCuuPhongDAO xl = new xlTraCuuPhongDAO();
            return xl.LoadLoaiPhong();
        }

        public DataTable LoadTinhTrang()
        {
            xlTraCuuPhongDAO xl = new xlTraCuuPhongDAO();
            return xl.LoadTinhTrang();
        }
        public DataTable LoadTraCuu()
        {
            xlTraCuuPhongDAO xl = new xlTraCuuPhongDAO();
            return xl.LoadTraCuu();
        }
    }
}
