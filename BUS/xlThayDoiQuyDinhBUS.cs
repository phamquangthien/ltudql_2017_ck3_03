﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class xlThayDoiQuyDinhBUS
    {
        public DataTable LoadThongTinLoaiPhong(string maloaiphong)
        {
            xlThayDoiQuyDinhDAO xl = new xlThayDoiQuyDinhDAO();
            return xl.LoadThongTinLoaiPhong(maloaiphong);
        }

        public DataTable LoadLoaiPhong()
        {
            xlThayDoiQuyDinhDAO xl = new xlThayDoiQuyDinhDAO();
            return xl.LoadLoaiPhong();
        }

        public DataTable LoadLoaiKhach()
        {
            xlThayDoiQuyDinhDAO xl = new xlThayDoiQuyDinhDAO();
            return xl.LoadLoaiKhach();
        }

        public DataTable LoadThongTinLoaiKhach(int maloaikhach)
        {
            xlThayDoiQuyDinhDAO xl = new xlThayDoiQuyDinhDAO();
            return xl.LoadThongTinLoaiKhach(maloaikhach);
        }

        public DataTable LoadThamSo(ThamSoDTO ts)
        {
            xlThayDoiQuyDinhDAO xl = new xlThayDoiQuyDinhDAO();
            return xl.LoadThamSo(ts);
        }
        public int SuaThongTinLoaiPhong(LoaiPhongDTO loai)
        {
            xlThayDoiQuyDinhDAO xl = new xlThayDoiQuyDinhDAO();
            return xl.SuaThongTinLoaiPhong(loai);
        }

        public int SuaHeSoKhach(LoaiKhachHangDTO lkh)
        {
            xlThayDoiQuyDinhDAO xl = new xlThayDoiQuyDinhDAO();
            return xl.SuaHeSoKhach(lkh);
        }

        public int SuaThamSo(ThamSoDTO ts)
        {
            xlThayDoiQuyDinhDAO xl = new xlThayDoiQuyDinhDAO();
            return xl.SuaThamSo(ts);
        }

        public bool IsNumber(string pValue)
        {
            xlThayDoiQuyDinhDAO xl = new xlThayDoiQuyDinhDAO();
            return xl.IsNumber(pValue);
        }
    }
}
