﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class ReportBUS
    {
        public DataTable DanhSachLoaiPhong()
        {
            ReportDAO xl = new ReportDAO();
            return xl.DanhSachLoaiPhong();
        }
        public DataTable PhieuCaoBaoDanhThu(string Thang)
        {
            ReportDAO xl = new ReportDAO();
            return xl.PhieuCaoBaoDanhThu(Thang);
        }
        public DataTable PhieuCaoBaoMatDo(string Thang)
        {
            ReportDAO xl = new ReportDAO();
            return xl.PhieuCaoBaoMatDo(Thang);
        }

        public DataTable BaoCaoDoanhThu(int month)
        {
            ReportDAO doanhThuDAO = new ReportDAO();
            return doanhThuDAO.getRevenue(month);
        }

        public DataTable BaoCaoMatDo(int month)
        {
            ReportDAO doanhThuDAO = new ReportDAO();
            return doanhThuDAO.getMatDo(month);
        }
        public string LayMaPhieuBaoCaoCuoiCung()
        {
            ReportDAO xl = new ReportDAO();
            DataTable dt = xl.LayMaPhieuBaoCaoCuoiCung();
            return dt.Rows[0][0].ToString();
        }
        public int ThemDataVaoPhieuBaoCao(PhieuCaoBaoDanhThu bc)
        {
            ReportDAO xl = new ReportDAO();
            return xl.ThemDataVaoPhieuBaoCao(bc);
        }
        public int ThemDataVaoPhieuBaoCaoMatDo(PhieuCaoBaoMatDo bc)
        {
            ReportDAO xl = new ReportDAO();
            return xl.ThemDataVaoPhieuBaoCaoMatDo(bc);
        }
        public string LayMaPhieuBaoCaoMatDOCuoiCung()
        {
            ReportDAO xl = new ReportDAO();
            DataTable dt = xl.LayMaPhieuBaoCaoMatDOCuoiCung();
            return dt.Rows[0][0].ToString();
        }
    }
}
