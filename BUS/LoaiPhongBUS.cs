﻿using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class LoaiPhongBUS
    {
        public DataTable loadLoaiPhong()
        {
            LoaiPhongDAO dao = new LoaiPhongDAO();

            return dao.loadLoaiPhong();
        }

    }
}
