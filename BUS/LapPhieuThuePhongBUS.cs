﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class LapPhieuThuePhongBUS
    {
        public Int32 ThemPhieuThuePhong(PhieuThuePhongDTO phieuThuePhong, List<ChiTietThuePhongDTO> dSChiTietThuePhongDTO)
        {
            LapPhieuThuePhongDAO lapPhieuThuePhongDAO = new LapPhieuThuePhongDAO();

            return lapPhieuThuePhongDAO.ThemPhieuThuePhong(phieuThuePhong, dSChiTietThuePhongDTO);
        }

        public int SoLuongKhachToiDaTrongPhong()
        {
            LapPhieuThuePhongDAO lapPhieuThuePhongDAO = new LapPhieuThuePhongDAO();

            return lapPhieuThuePhongDAO.SoLuongKhachToiDaTrongPhong();
        }
    }
}
