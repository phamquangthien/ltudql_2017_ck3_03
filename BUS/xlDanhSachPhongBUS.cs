﻿using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class xlDanhSachPhongBUS
    {
        public DataTable DanhSachPhong()
        {
            xlDanhSachPhongDAO xlDanhSachPhong = new xlDanhSachPhongDAO();
            return xlDanhSachPhong.DanhSachPhong();
        }
        public DataTable DanhSachPhongDaThue()
        {
            xlDanhSachPhongDAO xlDanhSachPhong = new xlDanhSachPhongDAO();
            return xlDanhSachPhong.DanhSachPhongDaThue();
        }
        public DataTable DanhSachPhongChuaThue()
        {
            xlDanhSachPhongDAO xlDanhSachPhong = new xlDanhSachPhongDAO();
            return xlDanhSachPhong.DanhSachPhongChuaThue();
        }
    }
}
