﻿using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class KhachHangBUS
    {
        public DataTable loadKhachHang()
        {
            KhachHangDAO khachHangDAO = new KhachHangDAO();

            return khachHangDAO.loadKhachHang();
        }
        public DataTable loadKhachHangDangThue(string MaPhong)
        {
            KhachHangDAO khachHangDAO = new KhachHangDAO();

            return khachHangDAO.loadKhachHangDangThue(MaPhong);
        }
    }
}
